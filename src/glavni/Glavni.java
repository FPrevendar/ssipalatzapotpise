package glavni;

import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import gui.GUI;


public class Glavni {
	
	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				GUI gui = new GUI();
				gui.setSize(600, 600);
				gui.setLocation(100, 100);
				gui.setTitle("Alat za zadavanje potpisa");
				gui.setVisible(true);
				gui.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			}
		});
	}

}
