package gui.TabZaCrtanje;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ProstorZaCrtanje extends JPanel {
	
	private List<GestaPoint> trenutniSegment;
	private List<List<GestaPoint>> potpis;
	
	public ProstorZaCrtanje() {
		trenutniSegment = new ArrayList<>();
		potpis = new ArrayList<>();
		setBackground(Color.WHITE);
		
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				noviSegment();
				trenutniSegment.add(new GestaPoint(e.getX(), e.getY()));
				repaint();
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				zavrsiSegment();
				repaint();
			}
		});
		
		this.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				trenutniSegment.add(new GestaPoint(e.getX(), e.getY()));
				repaint();
			}
		});
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setStroke(new BasicStroke(2));
		
		for(List<GestaPoint> segment : potpis) {
			for(int i = 0; i < segment.size()-1; i++) {
				g2d.drawLine(segment.get(i).x, segment.get(i).y, segment.get(i + 1).x, segment.get(i + 1).y);
			}
			if(segment.size() == 1) {
				g2d.drawLine(segment.get(0).x, segment.get(0).y, segment.get(0).x, segment.get(0).y);
			}
		}
		for(int i = 0; i < trenutniSegment.size()-1; i++) {
			g2d.drawLine(trenutniSegment.get(i).x, trenutniSegment.get(i).y, trenutniSegment.get(i + 1).x, trenutniSegment.get(i + 1).y);
		}
	}
	
	public void noviSegment() {
		trenutniSegment = new ArrayList<>();
	}
	
	public void zavrsiSegment() {
		potpis.add(trenutniSegment);
	}
	
	public List<List<GestaPoint>> dohvatiNacrtano(){
		return potpis;
	}
	
	public void pocisti() {
		potpis.clear();
		trenutniSegment.clear();
		repaint();
	};
}
