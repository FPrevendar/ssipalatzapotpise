package gui.TabZaCrtanje;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import gui.GUI;

@SuppressWarnings({"serial", "unused"})
public class DoljnjiIzbornik extends JPanel {

	private JButton spremiUDatotekuGumb;
	private JButton ocistiGumb;
	private JPanel donjiPanel;
	
	private GUI prozor;

	private ProstorZaCrtanje tabZaCrtanje;

	public DoljnjiIzbornik(ProstorZaCrtanje prostorZaCrtanje, GUI pr) {
		this.prozor = pr;
		this.tabZaCrtanje = prostorZaCrtanje;

		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		this.spremiUDatotekuGumb = new JButton();
		this.ocistiGumb = new JButton();


		this.donjiPanel = new JPanel();
		donjiPanel.setLayout(new GridLayout(1,2));
		donjiPanel.add(spremiUDatotekuGumb);
		donjiPanel.add(ocistiGumb);

		this.setLayout(new BorderLayout());
		this.add(donjiPanel, BorderLayout.SOUTH);


		spremiUDatotekuGumb.setAction(new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser(new File(System.getProperty("user.dir")));
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
						"txt datoteke", "txt");
				chooser.setFileFilter(filter);
				int returnVal = chooser.showOpenDialog(null);
				if(returnVal == JFileChooser.APPROVE_OPTION) {
					File izabraniFile = chooser.getSelectedFile();
					try {
						if(!izabraniFile.exists()) {
							izabraniFile.createNewFile();
						}
						PrintStream ps = new PrintStream(izabraniFile, "UTF-8");
						List<List<GestaPoint>> potpis = tabZaCrtanje.dohvatiNacrtano();
						ps.print(potpis.size() + "\n");
						for(List<GestaPoint> segment : potpis) {
							for(GestaPoint p : segment) {
								ps.print(p.x + " " + p.y + " ");
							}
							ps.print("\n");
						}
						ps.close();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				
				chooser = new JFileChooser(new File(System.getProperty("user.dir")));
				filter = new FileNameExtensionFilter(
						"png datoteke", "png");
				chooser.setFileFilter(filter);
				returnVal = chooser.showOpenDialog(null);
				if(returnVal == JFileChooser.APPROVE_OPTION) {
					File izabraniFile = chooser.getSelectedFile();
					try {
						if(!izabraniFile.exists()) {
							izabraniFile.createNewFile();
						}
						
						BufferedImage img = new BufferedImage(prostorZaCrtanje.getWidth(),
								prostorZaCrtanje.getHeight(),
								BufferedImage.TYPE_INT_RGB);
						prostorZaCrtanje.paint(img.getGraphics());
						ImageIO.write(img, "png", izabraniFile);
						
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				}
			});
		
		spremiUDatotekuGumb.setText("Spremi u datoteku");
		
		ocistiGumb.addActionListener(new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				prostorZaCrtanje.pocisti();
			}
		});
		ocistiGumb.setText("Ocisti");
		
		}
		
		private void ispisiPotpis() {
			List<List<GestaPoint>> potpis = tabZaCrtanje.dohvatiNacrtano();
			System.out.println(potpis.size());
			for(List<GestaPoint> segment : potpis) {
				for(GestaPoint p : segment) {
					System.out.print(p.x + " " + p.y + " ");
				}
				System.out.print("\n");
			}
		}

	}
