package gui.TabZaCrtanje;

import java.awt.BorderLayout;
import javax.swing.JPanel;

import gui.GUI;


@SuppressWarnings("serial")
public class TabZaCrtanje extends JPanel {
	
	private BorderLayout layout;
	private ProstorZaCrtanje prostorZaCrtanje;
	private DoljnjiIzbornik desniProstor;

	public TabZaCrtanje(GUI prozor) {
		this.prostorZaCrtanje = new ProstorZaCrtanje();
		this.desniProstor = new DoljnjiIzbornik(prostorZaCrtanje, prozor);
		
		layout = new BorderLayout();
		this.setLayout(layout);
		this.add(prostorZaCrtanje, BorderLayout.CENTER);
		this.add(desniProstor, BorderLayout.SOUTH);
	}
	
}
