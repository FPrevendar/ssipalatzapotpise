package gui;

import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;

import gui.TabZaCrtanje.GestaPoint;
import gui.TabZaCrtanje.TabZaCrtanje;

@SuppressWarnings("serial")
public class GUI extends JFrame{
	
	private JPanel tabZaCrtanje;
	
	private Map<String, List<List<GestaPoint>>> potpis;
	
	public GUI() {
		this.tabZaCrtanje = new TabZaCrtanje(this);
		this.add(tabZaCrtanje);
		
	}


	public Map<String, List<List<GestaPoint>>> getPotpis() {
		return potpis;
	}

	public void setPotpis(Map<String, List<List<GestaPoint>>> potpis) {
		this.potpis = potpis;
	}
	
}
